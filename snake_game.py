import time
import turtle
import random

ROZMIAR_PLANSZY = 600
LICZBA_SEGMENTOW_POCZATKOWYCH = 2
SZEROKOSC_ELEMENTU = 20
DODATKOWY_WYNIK_ZA_SEGMENT = 20

SEGMENTY = []


def dodaj_jedzenie(x, y):
    food = turtle.Turtle()
    food.speed(2)
    food.shape("square")
    food.color("red")
    food.penup()
    food.goto(x, y)
    return food


jedzenie = dodaj_jedzenie(100, 100)


def dodaj_segment(x, y, na_koniec=True):
    segment = turtle.Turtle()
    segment.speed(2)
    segment.shape("square")
    segment.color("green")
    segment.penup()
    segment.goto(x, y)
    global SEGMENTY
    if na_koniec:
        SEGMENTY.append(segment)
    else:
        SEGMENTY = [segment] + SEGMENTY
    return segment


def idz_do_gory():
    obecne_x = SEGMENTY[0].xcor()
    obecne_y = SEGMENTY[0].ycor()
    nowe_y = obecne_y + SZEROKOSC_ELEMENTU
    nowe_x = obecne_x
    dodaj_segment(nowe_x, nowe_y, na_koniec=False)
    SEGMENTY[-1].hideturtle()
    SEGMENTY.pop()
    print("Wciśnięto klawisz góra")


def idz_na_dol():
    obecne_x = SEGMENTY[0].xcor()
    obecne_y = SEGMENTY[0].ycor()
    nowe_y = obecne_y - SZEROKOSC_ELEMENTU
    nowe_x = obecne_x
    dodaj_segment(nowe_x, nowe_y, na_koniec=False)
    SEGMENTY[-1].hideturtle()
    SEGMENTY.pop()
    print("Wciśnięto klawisz dół")


def idz_w_lewo():
    obecne_x = SEGMENTY[0].xcor()
    obecne_y = SEGMENTY[0].ycor()
    nowe_x = obecne_x - SZEROKOSC_ELEMENTU
    nowe_y = obecne_y
    dodaj_segment(nowe_x, nowe_y, na_koniec=False)
    SEGMENTY[-1].hideturtle()
    SEGMENTY.pop()
    print("Wciśnięto klawisz lewo")


def idz_w_prawo():
    obecne_x = SEGMENTY[0].xcor()
    obecne_y = SEGMENTY[0].ycor()
    nowe_x = obecne_x + SZEROKOSC_ELEMENTU
    nowe_y = obecne_y
    dodaj_segment(nowe_x, nowe_y, na_koniec=False)
    SEGMENTY[-1].hideturtle()
    SEGMENTY.pop()
    print("Wciśnięto klawisz prawo")


def zainicjalizuj_plansze():
    ekran = turtle.Screen()
    ekran.title("Snake, wynik: 0")
    ekran.bgcolor("white")
    ekran.setup(width=ROZMIAR_PLANSZY, height=ROZMIAR_PLANSZY)
    ekran.tracer(0)
    ekran.onkeypress(idz_do_gory, "Up")
    ekran.onkeypress(idz_na_dol, "Down")
    ekran.onkeypress(idz_w_lewo, "Left")
    ekran.onkeypress(idz_w_prawo, "Right")
    ekran.listen()
    return ekran

def obecny_wynik():
    return (len(SEGMENTY) - LICZBA_SEGMENTOW_POCZATKOWYCH) * DODATKOWY_WYNIK_ZA_SEGMENT

ekran = zainicjalizuj_plansze()

def zderzenie():
    head = SEGMENTY[0]
    for segment in SEGMENTY[1:]:
        if nachodzenie(head, segment):
            return True
    return False


def nachodzenie(a, b):
    roznica_x = abs(a.xcor() - b.xcor())
    roznica_y = abs(a.ycor() - b.ycor())
    return roznica_x < SZEROKOSC_ELEMENTU and roznica_y < SZEROKOSC_ELEMENTU

def koniec_gry():
    ekran.clear()
    wynik = obecny_wynik()
    t = turtle.Turtle()
    t.hideturtle()
    t.write(f"Koniec gry!\n",align="center", font=("Arial", 16, "normal"))
    t.write(f"Twoj wynik to: {wynik} punktow",align="center", font=("Arial", 16, "normal"))
    time.sleep(4)
    ekran.bye()

for i in range(LICZBA_SEGMENTOW_POCZATKOWYCH):
    dodaj_segment(SZEROKOSC_ELEMENTU * i, 0)

while True:
    # tutaj dodajemy akcje
    if zderzenie():
        print("koniec gry")
        koniec_gry()
    glowa = SEGMENTY[0]
    glowa_x = glowa.xcor()
    glowa_y = glowa.ycor()
    glowa_p = SEGMENTY[0]
    glowa_k = SEGMENTY[-1]
    jedzenie_x = jedzenie.ycor()
    jedzenie_y = jedzenie.xcor()
    if jedzenie_y == glowa_x and jedzenie_x == glowa_y:
        jedzenie.hideturtle()
        random_x = random.randint(0, 20) * 20
        random_y = random.randint(0, 20) * 20
        jedzenie = dodaj_jedzenie(random_x, random_y)
        ostatni_segment = SEGMENTY[-1]
        dodaj_segment(ostatni_segment.xcor() - SZEROKOSC_ELEMENTU, ostatni_segment.ycor())
    ekran.update()
    time.sleep(0.08)
